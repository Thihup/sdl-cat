#pragma once

#include "SDL++.hpp"

namespace SDL{

	class Surface{
		SDL_Surface *sdlSurface;
	public:
		Surface(void);
		virtual ~Surface(void);

		SDL_Surface *operator *();
		SDL_Surface **operator &();

	};

}
