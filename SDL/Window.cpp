#include "Window.hpp"

namespace SDL{
	Window::Window(std::string title, int x, int y, int w, int h, Uint32 flags){
		sdlWindow = nullptr;
		sdlWindow = SDL_CreateWindow(title.c_str(), x, y, w, h, flags);
		if(sdlWindow == nullptr){
			throw SDL::Exception();
		}
	}

	Window::Window(void){
		sdlWindow = nullptr;
	}

	Window::~Window(void){
		if(sdlWindow != nullptr)
			SDL_DestroyWindow(sdlWindow);
	}
		
	void Window::setBordered(bool bordered){
		(bordered)?SDL_SetWindowBordered(sdlWindow, SDL_TRUE):SDL_SetWindowBordered(sdlWindow, SDL_FALSE);
	}

	int Window::setBrightness(float brightness){
		return SDL_SetWindowBrightness(sdlWindow, brightness);
	}

	void *Window::setData(std::string name, void *userdata){
		return SDL_SetWindowData(sdlWindow, name.c_str(), userdata);
	}

	int Window::setDisplayMode(SDL_DisplayMode *mode){
		return SDL_SetWindowDisplayMode(sdlWindow, mode);
	}

	int Window::setFullscreen(Uint32 flags){
		return SDL_SetWindowFullscreen(sdlWindow, flags);
	}

	int Window::setGammaRamp(const Uint16 *red, const Uint16 *green, const Uint16 *blue){
		return SDL_SetWindowGammaRamp(sdlWindow, red, green, blue);
	}

	void Window::setGrab(bool grabbed){
		(grabbed)?SDL_SetWindowGrab(sdlWindow, SDL_TRUE): SDL_SetWindowGrab(sdlWindow, SDL_FALSE);
	}

	int Window::setHitTest(SDL_HitTest callback, void *callback_data){
		return SDL_SetWindowHitTest(sdlWindow, callback, callback_data);
	}

	void Window::setIcon(SDL::Surface &icon){
		SDL_SetWindowIcon(sdlWindow, *icon);
	}

	void Window::setMaximumSize(int maxW, int maxH){
		SDL_SetWindowMaximumSize(sdlWindow, maxW, maxH);
	}

	void Window::setMinimumSize(int minW, int minH){
		SDL_SetWindowMinimumSize(sdlWindow, minW, minH);
	}

	void Window::setPosition(int x, int y){
		SDL_SetWindowPosition(sdlWindow, x, y);
	}

	void Window::setSize(int w, int h){
		SDL_SetWindowSize(sdlWindow, w, h);
	}

	void Window::setTitle(std::string title){
		SDL_SetWindowTitle(sdlWindow, title.c_str());
	}

	//SDL_Renderer *Window::getRenderer(){
	//	return SDL_GetRenderer(sdlWindow);
	//}

	void Window::glSwapWindow(){
		SDL_GL_SwapWindow(sdlWindow);
	}

	SDL_GLContext Window::glCreateContext(){
		return SDL_GL_CreateContext(sdlWindow);
	}

	SDL_Window **Window::operator &(){
		return &sdlWindow;
	}

	SDL_Window *Window::operator *(){
		return sdlWindow;
	}
}
