#include "Surface.hpp"

namespace SDL{

	Surface::Surface(void){
		sdlSurface = nullptr;
	}

	Surface::~Surface(void){
		if(sdlSurface != nullptr)
			SDL_FreeSurface(sdlSurface);
	}

	SDL_Surface *Surface::operator *(){
		return sdlSurface;
	}
	SDL_Surface **Surface::operator &(){
		return &sdlSurface;
	}

}