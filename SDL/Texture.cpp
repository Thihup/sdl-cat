#include "Texture.hpp"

namespace SDL{

	Texture::Texture(void){
		sdlTexture = nullptr;
	}

	Texture::Texture(Renderer &renderer,Uint32 format, int access, int w, int h){
		sdlTexture = nullptr;
		sdlTexture = SDL_CreateTexture(*renderer, format, access, w, h);
	}

	Texture::Texture(Renderer &renderer, Surface &surface){
		sdlTexture = nullptr;
		sdlTexture = SDL_CreateTextureFromSurface(*renderer, *surface);
	}

	Texture::~Texture(void){
		if(sdlTexture != nullptr)
			SDL_DestroyTexture(sdlTexture);
	}

	int Texture::query(Uint32 * format, int *access, int *w, int *h){
		return SDL_QueryTexture(sdlTexture, format, access, w, h);
	}


	int Texture::setColorMod(Uint8 r, Uint8 g, Uint8 b){return SDL_SetTextureColorMod(sdlTexture, r, g, b);}
	int Texture::getColorMod(Uint8 *r, Uint8 *g, Uint8 *b){return SDL_GetTextureColorMod(sdlTexture, r, g, b);}
	int Texture::setAlphaMod(Uint8 alpha){return SDL_SetTextureAlphaMod(sdlTexture, alpha);}
	int Texture::getAlphaMod(Uint8 *alpha){return SDL_GetTextureAlphaMod(sdlTexture, alpha);}

	int Texture::setBlendMode(SDL_BlendMode blendMode){return SDL_SetTextureBlendMode(sdlTexture, blendMode);}
	int Texture::getBlendMode(SDL_BlendMode *blendMode){return SDL_GetTextureBlendMode(sdlTexture, blendMode);}
	int Texture::update(const SDL_Rect * rect, const void *pixels, int pitch){return SDL_UpdateTexture(sdlTexture, rect, pixels, pitch);}
	int Texture::updateYUV(const SDL_Rect * rect, const Uint8 *Yplane, int Ypitch, const Uint8 *Uplane, int Upitch, const Uint8 *Vplane, int Vpitch){return SDL_UpdateYUVTexture(sdlTexture, rect, Yplane,Ypitch,Uplane,Upitch,Vplane, Vpitch);}
		
	int Texture::lock(const SDL_Rect * rect, void **pixels, int *pitch){return SDL_LockTexture(sdlTexture, rect, pixels, pitch);}
	void Texture::unlock(){SDL_UnlockTexture(sdlTexture);}

	int Texture::glBind(float *texw, float *texh){return SDL_GL_BindTexture(sdlTexture, texw, texh);}
	int Texture::glUnbind(){return SDL_GL_UnbindTexture(sdlTexture);}

	SDL_Texture *Texture::operator *(){
		return sdlTexture;
	}

	SDL_Texture **Texture::operator &(){
		return &sdlTexture;
	}
}
