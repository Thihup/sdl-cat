#pragma once

#include "SDL++.hpp"

namespace SDL{
	class Window{
		SDL_Window *sdlWindow;
	public:
		Window(std::string title, int x, int y, int w, int h, Uint32 flags);
		Window(void);
		virtual ~Window(void);
		
		void setBordered(bool bordered);
		int setBrightness(float brightness);
		void *setData(std::string name, void *userdata);
		int setDisplayMode(SDL_DisplayMode *mode);
		int setFullscreen(Uint32 flags);
		int setGammaRamp(const Uint16 *red, const Uint16 *green, const Uint16 *blue);
		void setGrab(bool grabbed);
		int setHitTest(SDL_HitTest callback, void *callback_data);
		void setIcon(Surface &icon);
		void setMaximumSize(int maxW, int maxH);
		void setMinimumSize(int minW, int minH);
		void setPosition(int x, int y);
		void setSize(int w, int h);
		void setTitle(std::string title);
		
		void glSwapWindow();
		SDL_GLContext glCreateContext();

		SDL_Window **operator &();
		SDL_Window *operator *();

	};

}

