#pragma once

#include "SDL++.hpp"

namespace SDL{

	class Renderer{
		SDL_Renderer *sdlRenderer;

	public:
		Renderer(Window &window, int index, Uint32 flags);
		Renderer();
		
		virtual ~Renderer(void);

		int getInfo(SDL_RendererInfo *info);
		int getOutputSize(int *w, int *h);
		bool targetSupported();
		int setLogicalSize(int w, int h);
		void getLogicalSize(int *w, int *h);
		int setViewport(const SDL_Rect *rect);
		void getViewport(SDL_Rect *rect);
		int setClipRect(const SDL_Rect *rect);
		void getClipRect(SDL_Rect *rect);
		bool isClipEnabled();
		int setScale(float scaleX, float scaleY);
		void getScale(float *scaleX, float *scaleY);
		int setDrawColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a);
		int getDrawColor(Uint8 *r, Uint8 *g, Uint8 *b, Uint8 *a);
		int setDrawBlendMode(SDL_BlendMode blendMode);
		int getDrawBlendMode(SDL_BlendMode *blendMode);
		int clear();
		int drawPoint(int x, int y);
		int drawPoints(const SDL_Point *points, int count);
		int drawLine(int x1, int y1, int x2, int y2);
		int drawLines(const SDL_Point *points, int count);
		int drawRect(const SDL_Rect *rect);
		int drawRects(const SDL_Rect *rects, int count);
		int fillRect(const SDL_Rect *rect);
		int fillRects(const SDL_Rect *rects, int count);
		int renderCopy(Texture &texture, const SDL_Rect *srcrect, const SDL_Rect * dstrect);
		int renderCopyEx(Texture &texture, const SDL_Rect *srcrect, const SDL_Rect *dstrect, const double angle, const SDL_Point *center, const SDL_RendererFlip flip);
		int readPixels(const SDL_Rect *rect,Uint32 format, void *pixels, int pitch);
		void present();


		SDL_Renderer **operator &();
		SDL_Renderer *operator *();

	};

}
