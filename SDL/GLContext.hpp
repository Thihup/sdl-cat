#pragma once

#include "SDL++.hpp"

namespace SDL{

	class GLContext{

	SDL_GLContext *sdlGlContext;
	public:
		GLContext(void);
		GLContext(SDL_GLContext context);
		virtual ~GLContext(void);

		void operator =(SDL_GLContext context);
		SDL_GLContext operator *();
		SDL_GLContext *operator &();
	};

}