#include "GLContext.hpp"

namespace SDL{

	GLContext::GLContext(void){
		sdlGlContext = nullptr;
	}

	GLContext::GLContext(SDL_GLContext context){
		sdlGlContext = &context;
	}

	GLContext::~GLContext(void){
		if(sdlGlContext != nullptr){
			SDL_GL_DeleteContext(*sdlGlContext);

		}
	}

	void GLContext::operator =(SDL_GLContext context){
		sdlGlContext = &context;
	}

	SDL_GLContext GLContext::operator *(){
		return *sdlGlContext;
	}
	
	SDL_GLContext *GLContext::operator &(){
		return sdlGlContext;
	}

}