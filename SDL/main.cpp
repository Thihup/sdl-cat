#include "SDL++.hpp"
#include <SDL_opengl.h>
#include <GL/glu.h>

#include <memory>

const int WIDTH = 640;
const int HEIGHT = 480;
const int RECT_SIZE = 100;

void draw(SDL_Rect a){
	glClearColor(255.0f, 255.0f, 255.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	glLoadIdentity();
	glTranslatef(a.x, a.y, 0);

	glColor3ub(255, 0, 0);

	glBegin(GL_QUADS);
		// Lado Superior Esquerdo
		glVertex2f(a.x,a.y);
		// Lado Superior Direito
		glVertex2f(a.x+a.w,a.y);
		// Lado Inferior Direito
		glVertex2f(a.x+a.w,a.y+a.h);
		// Lado Inferior Esquerdo
		glVertex2f(a.x,a.y+a.h);
	glEnd();

}

void initGL(){
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0,WIDTH, HEIGHT, 0); 
	glMatrixMode(GL_MODELVIEW);
	glClearColor(255.0f, 255.0f, 255.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
}


int main(int argc, char *argv[]){
	try{
	SDL_Init(SDL_INIT_EVERYTHING);

	std::unique_ptr<SDL::Window> window(new SDL::Window("Teste", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIDTH, HEIGHT, SDL_WINDOW_OPENGL));
	//std::unique_ptr<SDL::Renderer> renderer(new SDL::Renderer(*window, -1, 0));
	
	SDL::GLContext context = window->glCreateContext();

	initGL();

	bool isRunning = true;

	SDL_Event event;
	
	SDL_Rect rect = {WIDTH/2-RECT_SIZE, HEIGHT/2-RECT_SIZE, RECT_SIZE, RECT_SIZE};
	
	bool keyDown[4] = {false};
	
	while(isRunning){
		while(SDL_PollEvent(&event)){
			switch(event.type){
				case SDL_QUIT:
					isRunning = false;
				break;
				
				case SDL_KEYDOWN:
					switch(event.key.keysym.sym){
						case SDLK_UP:
							keyDown[0] = true;
						break;

						case SDLK_DOWN:
							keyDown[1] = true;
						break;						
						
						case SDLK_LEFT:
							keyDown[2] = true;
						break;
						
						case SDLK_RIGHT:
							keyDown[3] = true;
						break;												
						
					}
				break;
				
				case SDL_KEYUP:
					switch(event.key.keysym.sym){
						case SDLK_UP:
							keyDown[0] = false;
						break;

						case SDLK_DOWN:
							keyDown[1] = false;
						break;						
						
						case SDLK_LEFT:
							keyDown[2] = false;
						break;
						
						case SDLK_RIGHT:
							keyDown[3] = false;
						break;												
						
					}				
				break;
				
				default:break;
				
			}
		}
		
		if(keyDown[0]){
			rect.y -= 1;
		}

		if(keyDown[1]){
			rect.y += 1;
		}
		
		if(keyDown[2]){
			rect.x -= 1;
		}
		
		if(keyDown[3]){
			rect.x += 1;
		}
		
		draw(rect);
		window->glSwapWindow();


		//renderer->setRenderDrawColor(255, 255, 255, 255);
		//renderer->renderClear();
		
		//renderer->setRenderDrawColor(255, 0, 0, 255);
		//renderer->renderFillRect(&rect);
		//renderer->renderPresent();

	}

	SDL_Quit();
	}catch(SDL::Exception &e){
		printf("ERROR! %s\n",e.what());
	}
	return 0;
}
