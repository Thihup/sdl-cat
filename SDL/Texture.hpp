#pragma once

#include <SDL.h>

#include "SDL++.hpp"

namespace SDL{

	class Texture{
		SDL_Texture *sdlTexture;
	public:
		Texture(Renderer &renderer, Uint32 format, int access, int w, int h);
		Texture(Renderer &renderer, Surface &surface);
		Texture();
		
		int query(Uint32 * format, int *access, int *w, int *h);

		int setColorMod(Uint8 r, Uint8 g, Uint8 b);
		int getColorMod(Uint8 *r, Uint8 *g, Uint8 *b);
		int setAlphaMod(Uint8 alpha);
		int getAlphaMod(Uint8 *alpha);

		int setBlendMode(SDL_BlendMode blendMode);
		int getBlendMode(SDL_BlendMode *blendMode);
		int update(const SDL_Rect * rect, const void *pixels, int pitch);
		int updateYUV(const SDL_Rect * rect, const Uint8 *Yplane, int Ypitch, const Uint8 *Uplane, int Upitch, const Uint8 *Vplane, int Vpitch);
		
		int lock(const SDL_Rect * rect, void **pixels, int *pitch);
		void unlock();

		int glBind(float *texw, float *texh);
		int glUnbind();

		virtual ~Texture(void);

		SDL_Texture *operator *();
		SDL_Texture **operator &();

	};

}

