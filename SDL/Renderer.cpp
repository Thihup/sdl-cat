#include "Renderer.hpp"

namespace SDL{
	Renderer::Renderer(Window &window, int index, Uint32 flags){
		sdlRenderer = nullptr;
		sdlRenderer = SDL_CreateRenderer(*window, index, flags);
		/*
			Verificar se criou o renderer
		*/

		if(sdlRenderer == nullptr){
			throw SDL::Exception();
		}

	}

	Renderer::Renderer(){
		sdlRenderer = nullptr;
	}

	Renderer::~Renderer(void){
		if(sdlRenderer != nullptr)
			SDL_DestroyRenderer(sdlRenderer);
	}
	int Renderer::getInfo(SDL_RendererInfo *info){return SDL_GetRendererInfo(sdlRenderer, info);}
	int Renderer::getOutputSize(int *w, int *h){return SDL_GetRendererOutputSize(sdlRenderer, w, h);}
	bool Renderer::targetSupported(){return bool(SDL_RenderTargetSupported(sdlRenderer));}
	int Renderer::setLogicalSize(int w, int h){return SDL_RenderSetLogicalSize(sdlRenderer, w, h);}
	void Renderer::getLogicalSize(int *w, int *h){SDL_RenderGetLogicalSize(sdlRenderer, w, h);}
	int Renderer::setViewport(const SDL_Rect * rect){return SDL_RenderSetViewport(sdlRenderer, rect);}
	void Renderer::getViewport(SDL_Rect * rect){SDL_RenderGetViewport(sdlRenderer, rect);}
	int Renderer::setClipRect(const SDL_Rect * rect){return SDL_RenderSetClipRect(sdlRenderer, rect);}
	void Renderer::getClipRect(SDL_Rect * rect){SDL_RenderGetClipRect(sdlRenderer, rect);}
	bool Renderer::isClipEnabled(){return bool(SDL_RenderIsClipEnabled(sdlRenderer));}
	int Renderer::setScale(float scaleX, float scaleY){return SDL_RenderSetScale(sdlRenderer, scaleX, scaleY);}
	void Renderer::getScale(float *scaleX, float *scaleY){SDL_RenderGetScale(sdlRenderer, scaleX, scaleY);}
	int Renderer::setDrawColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a){return SDL_SetRenderDrawColor(sdlRenderer, r, g, b, a);}
	int Renderer::getDrawColor(Uint8 * r, Uint8 * g, Uint8 * b, Uint8 * a){return SDL_GetRenderDrawColor(sdlRenderer, r, g, b, a);}
	int Renderer::setDrawBlendMode(SDL_BlendMode blendMode){return SDL_SetRenderDrawBlendMode(sdlRenderer, blendMode);}
	int Renderer::getDrawBlendMode(SDL_BlendMode *blendMode){return SDL_GetRenderDrawBlendMode(sdlRenderer, blendMode);}
	int Renderer::clear(){return SDL_RenderClear(sdlRenderer);}
	int Renderer::drawPoint(int x, int y){return SDL_RenderDrawPoint(sdlRenderer, x, y);}
	int Renderer::drawPoints(const SDL_Point * points, int count){return SDL_RenderDrawPoints(sdlRenderer, points, count);}
	int Renderer::drawLine(int x1, int y1, int x2, int y2){return SDL_RenderDrawLine(sdlRenderer, x1, y1, x2, y2);}
	int Renderer::drawLines(const SDL_Point * points, int count){return SDL_RenderDrawPoints(sdlRenderer, points, count);}
	int Renderer::drawRect(const SDL_Rect * rect){return SDL_RenderDrawRect(sdlRenderer, rect);}
	int Renderer::drawRects(const SDL_Rect * rects, int count){return SDL_RenderDrawRects(sdlRenderer, rects, count);}
	int Renderer::fillRect(const SDL_Rect * rect){return SDL_RenderFillRect(sdlRenderer, rect);}
	int Renderer::fillRects(const SDL_Rect * rects, int count){return SDL_RenderFillRects(sdlRenderer, rects, count);}
	int Renderer::renderCopy(Texture &texture, const SDL_Rect * srcrect, const SDL_Rect * dstrect){return SDL_RenderCopy(sdlRenderer, *texture, srcrect, dstrect);}
	int Renderer::renderCopyEx(Texture &texture, const SDL_Rect * srcrect, const SDL_Rect * dstrect, const double angle, const SDL_Point *center, const SDL_RendererFlip flip){return SDL_RenderCopyEx(sdlRenderer, *texture,srcrect, dstrect,angle,center,flip);}
	int Renderer::readPixels(const SDL_Rect * rect,Uint32 format, void *pixels, int pitch){return SDL_RenderReadPixels(sdlRenderer, rect,format,pixels,pitch);}
	void Renderer::present(){SDL_RenderPresent(sdlRenderer);}

	SDL_Renderer **Renderer::operator &(){
		return &sdlRenderer;
	}

	SDL_Renderer *Renderer::operator *(){
		return sdlRenderer;
	}

}
