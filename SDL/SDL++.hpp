#pragma once

#include <SDL.h>
#include <string>

namespace SDL{
	class Window;
	class Renderer;
	class Surface;
	class Texture;
	class GLContext;
	class Exception;
}

#include "Window.hpp"
#include "Renderer.hpp"
#include "Surface.hpp"
#include "Texture.hpp"
#include "GLContext.hpp"
#include "Exception.hpp"