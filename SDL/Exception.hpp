#pragma once

#include <string>
#include <exception>

#include <SDL.h>

namespace SDL{
	class Exception: public std::runtime_error {
	  public:
		Exception(): std::runtime_error(SDL_GetError()) { }
		Exception(const std::string &msg): std::runtime_error(msg) { }
	};
}
